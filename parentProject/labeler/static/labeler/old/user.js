/*jslint browser: true, devel: true */
$(document).ready(function () {
    "use strict";
    const csrftoken = Cookies.get("csrftoken");  // token for post using cookies.js library

    /**
     * Display toast message for duration of time
     * @param message message to be displayed
     * @param duration duration of the message in ms
     */
    function displayToast(message, duration) {
        const d = document.getElementById("snackbar");
        d.className = "show";
        d.innerHTML = message;
        setTimeout(function () {
            d.className = d.className.replace("show", "");
        }, duration);
    }


    /**
     * Adjust the user credits and call the toast function with the returned data
     * @param self - the list object whos vaue will be updated
     * @param data - the callback data from the backend
     */
    function buyCallback(self, data) {
        if (data.ok) {
            self.text(data.value);
            document.getElementById("user-stat-credits").innerHTML = "Credits: " + data.credits;
        }
        displayToast(data.toast, 3000);
    }

    $("#skill-suggestion").click(function () {
        const self = $(this);
        $.post("/api/skill/suggestions/").done(function (data) {
            buyCallback(self, data);
        });
    });

    $("#skill-combo").click(function () {
        const self = $(this);
        $.post("/api/skill/combo/").done(function (data) {
            buyCallback(self, data);
        });
    });

    $("#skill-video-speed-multiplier").click(function () {
        const self = $(this);
        $.post("/api/skill/video_speed_multiplier/").done(function (data) {
            buyCallback(self, data);
        });
    });

    $("#skill-credits-multiplier").click(function () {
        const self = $(this);
        $.post("/api/skill/credits_multiplier/").done(function (data) {
            buyCallback(self, data);
        });
    });

    $("#skill-experience-multiplier").click(function () {
        const self = $(this);
        $.post("/api/skill/experience_multiplier/").done(function (data) {
            buyCallback(self, data);
        });
    });


    /*
    Additional methods; cookies
     */
    // Set the crsf cookie
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});