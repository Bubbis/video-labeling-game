$(document).ready(function(){
    "use strict";
    const csrftoken = Cookies.get("csrftoken");  // token for post using cookies.js library
    const video = document.getElementById("videoPlayer");  // Video player
    const data = JSON.parse(document.getElementById("data").textContent);  // JSON data from the server
    const video_original_start = data.label.startTime;  // The original video start, used for to correctly label times
    const video_original_end = data.label.endTime;  // The original video end, used for to correctly label times
    const labelsContainer = document.getElementById("labels-container");  // Container for the view
    const sliderContainer = document.getElementById("sliderContainer");  // Container for the sliders
    let labels = [];  // List of labels; when new ones are created the list is appended
    let sliders = [];  // List of sliders, corresponding to labels


    /**
     * Creates slider from 0 to length(:end-start) and appends to container.
     * Creates also event listeners for the buttons created
     * @param container - Container of sliders
     * @param length - length of the slider
     * @param label - label object that is used for the eventListener on the buttons
     * @returns {HTMLElement} - the slider element
     */
    function createSlider(container, length, label){
        const child_container = document.createElement("div");  // Child container for buttons + slider
        child_container.setAttribute("id","slider-child-container");
        const al_button = document.createElement("div");  // Add time to left side of the bar
        const ar_button = document.createElement("div");  // Add time to right side of the bar
        const c_seek_div = document.createElement("div"); // Custom seek bar div
        const c_seek_span = document.createElement("span");  // Custom seek bar span
        c_seek_div.appendChild(c_seek_span);
        c_seek_div.setAttribute("id", "custom-seekbar");
        al_button.setAttribute("class", "fas fa-step-forward fa-2x");
        al_button.setAttribute("id", "add-left-button");
        ar_button.setAttribute("class", "fas fa-step-backward fa-2x");
        ar_button.setAttribute("id", "add-right-button");
        const slider = document.createElement("div");
        noUiSlider.create(slider, {
            start: [0, length],
            margin: 1.0,
            behaviour: "snap",
            step: 0.2,
            range: {
                "min": [0],
                "max": [length]
            },
            connect: true,
            tooltips: true
        });
        slider.appendChild(c_seek_div);
        child_container.appendChild(al_button);
        child_container.appendChild(slider);
        child_container.appendChild(ar_button);
        container.appendChild(child_container);
        al_button.addEventListener("click", function(){
           if (!("add_left" in label)){
               label.add_left = true;
               al_button.setAttribute("class", "fas fa-step-backward fa-2x");
           }
           else{
               if (label.add_left){
                   label.add_left = false;
                   al_button.setAttribute("class", "fas fa-step-forward fa-2x");
               }
               else {
                   label.add_left = true;
                   al_button.setAttribute("class", "fas fa-step-backward fa-2x");
               }
           }
        });
        ar_button.addEventListener("click", function(){
            if (!("add_right" in label)){
                label.add_right = true;
                ar_button.setAttribute("class", "fas fa-step-forward fa-2x");
            }
            else{
                if (label.add_right){
                    label.add_right = false;
                    ar_button.setAttribute("class", "fas fa-step-backward fa-2x");
                }
                else{
                    label.add_right = true;
                    ar_button.setAttribute("class", "fas fa-step-forward fa-2x");
                }
            }
        });
        return slider;
    }

    /**
     * Initialize the autocomplete for label input field
     * @param input - input field with three models
     * @param svo - subject, verb, object
     */
    function init_autocomplete(input, svo){
        const datalist = document.createElement("datalist");
        datalist.setAttribute("id",svo);
        input.setAttribute("list", svo);
        input.addEventListener("input", function(){
            if (this.value.length === 1) {  // Request only the list from the first letter
                let url = "/api/suggest/"+svo+"/"+this.value+"/"; // Url to send the GET
                $.get(url).done(function(data){
                    datalist.innerHTML = "";
                    create_list(data);
                });
            }
        });

        function create_list(items){
            for (let index=0; index < items.length; index++){
                const item = document.createElement("option");
                item.setAttribute("value", items[index]);
                datalist.append(item);
            }
            $(datalist).insertAfter(input);
        }
    }

    /**
     * Create label form and attach listeners corresponding to label object
     * Defaults to values = null
     * @param container - Container which the form is appended to
     * @param label - Label json object that is updated onchange
     * @returns {HTMLElement} - form element
     */
    function createLabelForm(container, label, s=null, v=null, o=null){
        const form = document.createElement("form");
        form.setAttribute("id", "label-form");
        const subject = document.createElement("input");
        [["type","text"],["name","subject"],["placeholder", "subject"]].forEach(function(element){
            subject.setAttribute(element[0],element[1]);
        });
        if(s !== null) {subject.setAttribute("value", s);}
        subject.addEventListener("change",function(){
            label.subject = subject.value;
        });
        const verb = document.createElement("input");
        [["type","text"],["name","subject"],["placeholder", "action"]].forEach(function(element){
            verb.setAttribute(element[0],element[1]);
        });
        if(v !== null){verb.setAttribute("value", v);}
        verb.addEventListener("change", function () {
            label.verb = verb.value;
        });
        const object = document.createElement("input");
        [["type","text"],["name","subject"],["placeholder", "object"]].forEach(function(element){
            object.setAttribute(element[0],element[1]);
        });
        if(o !== null){object.setAttribute("value", o);}
        object.addEventListener("change", function () {
            label.object = object.value;
        });
        [subject, verb, object].forEach(function (element) {
            form.appendChild(element);
        });
        const remove_button = document.createElement("div");  // Button to remove form
        remove_button.setAttribute("class", "fas fa-minus-circle");
        remove_button.setAttribute("id", "remove-button");
        form.appendChild(remove_button);
        container.appendChild(form);
        // Event listener for autocomplete
        init_autocomplete(subject, "subject");
        init_autocomplete(verb, "verb");
        init_autocomplete(object, "object");
        return form;
    }

    /**
     * Callback for removing slider, label and corresponding form
     * @param labels - list of labels
     * @param label - label to be removed from list of labels
     * @param sliders - list of sliders
     * @param slider - slider to be removed from list of sliders
     * @param form - form to be deleted
     * @returns {number} - 0 on success 1 on error (Not trustworthy)
     */
    function removeLabelSliderForm(labels, label, sliders, slider, form){
        if (labels.length === sliders.length) {
            for (const [i, v] of labels.entries()) {
                if (v.id === label.id) {
                    sliders.splice(i, 1);  // Remove the slider
                    labels.splice(i, 1);  // Remove the label
                }
            }
        }
        else {
            return 1;  // Error
        }
        $(slider.parentNode).remove();
        slider.noUiSlider.destroy();  // Call the destructor for slider
        form.parentNode.removeChild(form);  // Call the destructor for the form
        return 0 // Success
    }

    /**
     * Update labels according to slider values
     * Note: Labels and sliders need to be in the same order
     * @param labels - list of labels
     * @param sliders - list of sliders
     * @param offset_start - The clips start on the orginal video. (: Used as offset)
     * @param offset_end - The clips end on the orginal video. (: Used as offset)
     */
    function setTimes(labels, sliders, offset_start, offset_end) {
        const length = offset_end - offset_start;
        if (labels.length === sliders.length) {
            for (let i = 0; i < labels.length; i = i+1) {
                let values = sliders[i].noUiSlider.get();
                labels[i].startTime = offset_start + parseFloat(values[0]);
                labels[i].endTime = offset_end - (length - parseFloat(values[1]));
            }
        } else{console.log("Sliders and labels did not match.. Could not proceed");}
    }

    /**
     * Sets video to same time as slider
     * Note: Used as callback for slider event
     * @param values - Values for the slider handles
     * @param handle - Handle number (0..n) n=number of handlers
     * @param unencoded - Slider values without formatting (array);
     * @param tap - Event was caused by the user tapping the slider (boolean);
     * @param positions - Left offset of the handles (array);
     */
    function setVideoTime(values, handle, unencoded, tap, positions) {
        if(handle === 0){ // Start changes
            video.currentTime = values[0];
        }
        else{
            video.currentTime = values[1];
        }
    }

    // Initialize the original label with the form and sliders:
    labels.push(data.label);
    sliders.push(createSlider(sliderContainer, data.label.endTime - data.label.startTime, data.label));
    sliders[0].noUiSlider.on("change", setVideoTime);
    const form = createLabelForm(labelsContainer, data.label, data.label.subject, data.label.verb, data.label.object);
    form.getElementsByTagName("div")[0].parentNode.removeChild(form.getElementsByTagName("div")[0]);  // Delete remove button

    /*
    This section is for event handlers on buttons
     */
    $("#play-pause").click(function(){
       if (video.paused){
           video.play();
           $("#play-pause").attr("class","fas fa-pause-circle fa-2x")
       }
       else{
           video.pause();
           $("#play-pause").attr("class","fas fa-play-circle fa-2x")
       }
    });

    $("video").click(function(){
        if (video.paused){
            video.play();
            $("#play-pause").attr("class","fas fa-pause-circle fa-2x")
        }
        else{
            video.pause();
            $("#play-pause").attr("class","fas fa-play-circle fa-2x")
        }
    });

    $("#mute-unmute").click(function () {
        if(video.muted){
            video.muted = false;
            $("#mute-unmute").attr("class","fas fa-volume-up fa-2x");
        }
        else{
            video.muted = true;
            $("#mute-unmute").attr("class", "fas fa-volume-mute fa-2x");
        }
    });

    $("#speed-multiplier").click(function () {
        if(video.playbackRate === 1 && data.skills.video_speed_multiplier > 1){
            video.playbackRate = 2;
            $("#speed-multiplier").html("2x");
        }
        else if(video.playbackRate === 2 && data.skills.video_speed_multiplier > 2){
            video.playbackRate = 4;
            $("#speed-multiplier").html("4x");
        }
        else{
            if (data.skills.video_speed_multiplier == 1){
                displayToast("You don't have this skill", 3000);
            }
            video.playbackRate = 1;
            $("#speed-multiplier").html("1x");
        }
    });

    $("#createLabel").click(function(){
        const label = {
            id: labels[labels.length - 1].id + 1,  // Increment the label by one
            startTime: video_original_start,
            endTime: video_original_end,
            subject: null,
            verb: null,
            object: null,
            video: labels[labels.length - 1].video,  // Get the video id from the last label
        }
        labels.push(label);  // Add newly created label to the list
        const slider = createSlider(sliderContainer, data.label.endTime - data.label.startTime, label)
        sliders.push(slider);
        sliders[sliders.length - 1].noUiSlider.on("change", setVideoTime);  // Listen for slider values change
        const form = createLabelForm(labelsContainer, label);  // Make empty form and attach listeners for label
        const btn = form.getElementsByTagName("div");  // Get all buttons created by the form; [0] remove
        btn[0].addEventListener("click", function(){  // Listener for deleting the created label
           removeLabelSliderForm(labels, label, sliders, slider, form);
        });
    });

    $("#postLabels").click(function(){
       setTimes(labels, sliders, video_original_start, video_original_end);
       const rd = labels.reduce((obj, item) => {
           obj[item.id] = item;
           return obj
       }, {});
       $.post($(location).attr("pathname"),JSON.stringify(rd)).done(function(data){
           window.location.href = data.url;
       });
    });

    $("#print-data").click(function(){
       console.log(data);
    });

    video.ontimeupdate = function(){
        var percentage = ( video.currentTime / video.duration ) * 100;
        $("#custom-seekbar span").css("width", percentage+"%");
    };

    video.onended = function(){  // Change the custom play-pause button when video ends
        $("#play-pause").attr("class","fas fa-play-circle fa-2x");
    };

    /**
     * Display toast message for duration of time
     * @param message message to be displayed
     * @param duration duration of the message in ms
     */
    function displayToast(message, duration){
        const d = document.getElementById("snackbar");
        d.className = "show";
        d.innerHTML = message;
        setTimeout(function(){
            d.className = d.className.replace("show","");
        }, duration)
    }

    /*
    Additional methods; cookies
     */
    // Set the crsf cookie
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});