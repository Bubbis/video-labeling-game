/*jslint browser: true, devel: true */
$(document).ready(function () {
    "use strict";
    const csrftoken = Cookies.get("csrftoken");
    const data = JSON.parse(document.getElementById("data").textContent);  // JSON data from the server
    const video = document.getElementById("videoPlayer");  // Video player

    /**
     * Display toast message for duration of time
     * @param message message to be displayed
     * @param duration duration of the message in ms
     */
    function displayToast(message, duration) {
        const d = document.getElementById("snackbar");
        d.className = "show";
        d.innerHTML = message;
        setTimeout(function () {
            d.className = d.className.replace("show", "");
        }, duration);
    }
    /*
    This section is for event handlers on buttons
     */
    $("#no").click(function () {
        $.post($(location).attr("pathname"), JSON.stringify({vote: "-1", labelId: data.label.id})).done(
            function (data) {
                window.location.href = data.url;
            }
        );
    });

    $("#yes").click(function () {
        $.post($(location).attr("pathname"), JSON.stringify({vote: "1", labelId: data.label.id})).done(
            function (data) {
                window.location.href = data.url;
            }
        );
    });

    /** Custom seek bar click action
     * https://stackoverflow.com/questions/41953604/html5-video-custom-additional-seek-bar
     */
    $("#custom-seekbar").on("click", function (e) {
        const offset = $(this).offset();
        const left = (e.pageX - offset.left);
        const totalWidth = $("#custom-seekbar").width();
        const percentage = (left / totalWidth);
        const videoTime = video.duration * percentage;
        video.currentTime = videoTime;
    });

    $("#play-pause").click(function () {
        if (video.paused) {
            video.play();
            $("#play-pause").attr("class", "fas fa-pause-circle fa-2x");
        } else {
            video.pause();
            $("#play-pause").attr("class", "fas fa-play-circle fa-2x");
        }
    });

    $("video").click(function () {
        if (video.paused) {
            video.play();
            $("#play-pause").attr("class", "fas fa-pause-circle fa-2x");
        } else {
            video.pause();
            $("#play-pause").attr("class", "fas fa-play-circle fa-2x");
        }
    });

    $("#mute-unmute").click(function () {
        if (video.muted) {
            video.muted = false;
            $("#mute-unmute").attr("class", "fas fa-volume-up fa-2x");
        } else {
            video.muted = true;
            $("#mute-unmute").attr("class", "fas fa-volume-mute fa-2x");
        }
    });

    $("#speed-multiplier").click(function () {
        if (video.playbackRate === 1 && data.skills.video_speed_multiplier > 1) {
            video.playbackRate = 2;
            $("#speed-multiplier").html("2x");
        } else if (video.playbackRate === 2 && data.skills.video_speed_multiplier > 2) {
            video.playbackRate = 4;
            $("#speed-multiplier").html("4x");
        } else {
            if (data.skills.video_speed_multiplier === 1) {
                displayToast("You don't have this skill", 3000);
            }
            video.playbackRate = 1;
            $("#speed-multiplier").html("1x");
        }
    });

    video.ontimeupdate = function () {
        var percentage = (video.currentTime / video.duration) * 100;
        $("#custom-seekbar span").css("width", percentage + "%");
    };


    /*
    Additional methods; cookies
    */
    // Set the crsf cookie
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});