$(document).ready(function(){
    const scoreboard = document.getElementById("scoreboard");
    const $scoreboard = $("#scoreboard");
    let rankings = true;
    const high_scores_button = document.getElementById("high_scores_button");
    const rankings_button = document.getElementById("rankings_button");


    $scoreboard.click(function(event){
        const username = event.target.parentNode.childNodes[1].innerHTML;
        $scoreboard.fadeOut(function(){
            playerOverviewController.display(username);
        })
    });

    high_scores_button.addEventListener("click", function(){
        $scoreboard.fadeOut(function(){
            rankings_button.removeAttribute("disabled");
            high_scores_button.setAttribute("disabled","");
            highScoresController.render();
        });
    });

    rankings_button.addEventListener("click", function(){
        rankings_button.setAttribute("disabled", "");
        high_scores_button.removeAttribute("disabled");
        highScoresController.hide(function () {
            $scoreboard.slideDown();
        });
    });

    function fetch_and_render(scoreboard){
        $.get("/api/scoreboard/10/").done(function(data){
            for (let key in data){
                let temp = document.createElement("tr");
                let rank = document.createElement("td");
                rank.innerHTML = parseInt(key) + 1;
                temp.appendChild(rank);
                let user = document.createElement("td");
                user.setAttribute("id", "username_id");
                user.innerHTML = data[key].name;
                temp.appendChild(user);
                let level = document.createElement("td");
                level.innerHTML = data[key].level;
                temp.appendChild(level);
                scoreboard.appendChild(temp);
            }
        });
    }
    fetch_and_render(scoreboard);

    const highScoresController = (function(){
        let high_scores = document.getElementById("high_scores");
        let $high_scores = $("#high_scores");

        function fetch(){
            $.get("/api/high_scores/10/").done(function(data){
                for(let key in data){
                    let temp = document.createElement("tr");
                    let rank = document.createElement("td");
                    rank.innerHTML = parseInt(key) + 1;
                    temp.appendChild(rank);
                    let user = document.createElement("td");
                    user.setAttribute("id", "username_id");
                    user.innerHTML = data[key].name;
                    temp.appendChild(user);
                    let high_score = document.createElement("td");
                    high_score.innerHTML = data[key].high_score;
                    temp.appendChild(high_score);
                    high_scores.appendChild(temp);
                }
            })
        }

        function render(){
            $high_scores.fadeIn();
        }

        function hide(callback){
            $high_scores.fadeOut(callback);
        }
        return {
            fetch: fetch,
            render: render,
            hide: hide
        }
    })();


    highScoresController.fetch()

    const playerOverviewController = (function(){
        let $player = $("#player-overview");
        const username = document.getElementById("username");
        const level = document.getElementById("level");
        const clicks = document.getElementById("clicks");
        const accuracy = document.getElementById("accuracy");
        const credits = document.getElementById("credits");

        function display(username){
            if(username === username.innerHTML){_render();}
            else{
                $.get("/api/player/"+username+"/").done(function(data){
                    if(data.found === true){
                        _update_fields_and_show(data.username, data.level, data.contributions,
                            data.accuracy, data.credits)
                    }
                })
            }
        }

        function _render(){
            $player.slideDown();
            high_scores_button.setAttribute("disabled","");
            $player.click(function(){
                $player.slideUp(function(){
                    high_scores_button.removeAttribute("disabled");
                    $scoreboard.fadeIn();
                })
            })
        }

        function _update_fields_and_show(user, lev, cli, acc, cred){
            username.innerHTML = user;
            level.innerHTML = "Level "+lev;
            clicks.innerHTML = "Clicks "+cli;
            accuracy.innerHTML = "Accuracy "+acc;
            credits.innerHTML = cred+"$";
            _render();
        }

        return {
            display: display
        }

    })();
});