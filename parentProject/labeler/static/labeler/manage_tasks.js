$(document).ready(function(){
    const csrftoken = Cookies.get("csrftoken");  // token for post using cookies.js library
    const video = document.getElementById("videoPlayer");  // Video player
    const container = document.getElementById("video-labels-container");
    const container_button = document.getElementById("manage-tasks-container");
    const $container = $("#video-labels-container");
    let forms = [];
    let video_id;
    let video_length;
    let questions;

    /**
     * Post the modifications to the server for the tasks.
     * Previous tasks are deleted and new ones are created accordingly.
     * NOTE: This removes all previous answers & tasks for the video
     */
    function postForms(){
        // Start console.log(forms[i].childNodes[0].value);
        // End console.log(forms[i].childNodes[1].value);
        // Question console.log(forms[i].childNodes[4].value);
        let d = {};
        for (let i = 0; i < forms.length ; i = i + 1){
            d[i] = {
                "start": forms[i].childNodes[0].value,
                "end": forms[i].childNodes[1].value,
                "question": forms[i].childNodes[4].value
            }
        }
        let send_data = JSON.stringify(d);
        $.post("/api/manage/task/"+video_id+"/", send_data).done(function(data){
            if (data.status){displayToast("Updated",3000)}
            else{displayToast("Error", 3000)}
        })
    }

    /**
     * Create submit button for all forms
     */
    function createSubmit(){
        const submit_button = document.createElement("input");
        submit_button.setAttribute("type", "button");
        submit_button.setAttribute("value","Update");
        submit_button.setAttribute("class", "btn btn-secondary");
        submit_button.setAttribute("id", "submit");
        submit_button.addEventListener("click", postForms);
        container_button.appendChild(submit_button);
    }

    function deleteLastForm(){
        let form = forms.pop();
        form.parentNode.removeChild(form);
    }

    function createDelete(){
        const delete_button = document.createElement("input");
        delete_button.setAttribute("type", "button");
        delete_button.setAttribute("class", "btn btn-secondary");
        delete_button.setAttribute("value","Delete last");
        delete_button.setAttribute("id", "delete");
        delete_button.addEventListener("click", deleteLastForm);
        container_button.appendChild(delete_button);
    }


    function createCreate(){
        const create_button = document.createElement("input");
        create_button.setAttribute("type", "button");
        create_button.setAttribute("value","New task");
        create_button.setAttribute("class", "btn btn-secondary");
        create_button.setAttribute("id", "create");
        create_button.addEventListener("click", function(){createForm(video.currentTime)});
        container_button.appendChild(create_button);
    }

    function setStart(event){
        this.setAttribute("value", video.currentTime);
    }

    function setEnd(event){
        this.setAttribute("value", video.currentTime);
    }


    /**
     *
     * @param s - start time
     * @param e - end time
     * @param selected - selected question
     */
    function createForm(s=0, e=video_length, selected=false){
        let form = document.createElement("form");
        form.setAttribute("action", "/api/manage/task/"+video_id+"/");
        let start = document.createElement("input");
        start.setAttribute("type", "number");
        start.setAttribute("value", s);
        start.setAttribute("id", "start-time");
        let end = document.createElement("input");
        end.setAttribute("type","number");
        end.setAttribute("value", e);
        end.setAttribute("id", "end-time");
        form.appendChild(start);
        form.appendChild(end);

        let set_start_button = document.createElement("input");
        set_start_button.setAttribute("type","button");
        set_start_button.setAttribute("value","Set Start");
        set_start_button.setAttribute("class", "btn btn-secondary btn-sm");
        set_start_button.setAttribute("id", "set-start");
        set_start_button.addEventListener("click", setStart.bind(start));
        form.appendChild(set_start_button);

        let set_end_button = document.createElement("input");
        set_end_button.setAttribute("type", "button");
        set_end_button.setAttribute("value", "Set End");
        set_end_button.setAttribute("class", "btn btn-secondary btn-sm");
        set_end_button.setAttribute("id", "set-end");
        set_end_button.addEventListener("click", setEnd.bind(end));
        form.appendChild(set_end_button);

        let select = document.createElement("select");
        for (let i = 0; i < questions.length; i = i + 1){
            let temp = document.createElement("option");
            temp.setAttribute("value", questions[i]);
            temp.innerHTML = questions[i];
            if(selected===questions[i]){
                temp.selected = true;
            }
            select.appendChild(temp);
        }
        form.appendChild(select);
        forms.push(form);
        container.appendChild(form);
    }

    /*
    This section is for event handlers on buttons
     */
    $(".list-group-item").click(function(){
        $container.show();
        video_id = $(this).attr('id');
        $.get("/api/manage/task/"+video_id+"/", function(data){
            video.setAttribute("src", data.src);
            video_length = data.length;
            if (questions !== false){
                questions = data.questions;
            }
            else{
                questions = [];
            }
            if (data.tasks !== false){
                for (let i = 0; i < data.tasks.length; i = i + 1){
                    createForm(data.tasks[i].start, data.tasks[i].end, data.tasks[i].question);
                }
            }
        }).then(function(){
            createCreate();
            createDelete();
            createSubmit();
        });
    });


    /**
     * Display toast message for duration of time
     * @param message message to be displayed
     * @param duration duration of the message in ms
     */
    function displayToast(message, duration){
        const d = document.getElementById("snackbar");
        d.className = "show";
        d.innerHTML = message;
        setTimeout(function(){
            d.className = d.className.replace("show","");
        }, duration)
    }

    /*
    Additional methods; cookies
     */
    // Set the crsf cookie
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});