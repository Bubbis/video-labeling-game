$(document).ready(function(){
    "use strict";
    const csrftoken = Cookies.get("csrftoken");  // token for post using cookies.js library
    //const video = document.getElementById("videoPlayer");
    const video = document.createElement("video");
    const question = document.getElementById("question-container");
    const $question_container = $("#question-container");
    const $question_root_container = $("#question-root-container");
    const $video_labels_container = $("#video-labels-container");
    let question_key = null;
    let segment_info = {};
    let accept_clicks = true;
    let accept_toast = true;
    const volume_button = document.getElementById("volume-button");

    /**
     * Controls the video play pause action. TODO: Other stuff of video..
     * @type {{play, pause}}
     */
    const videoController = (function(){
        const $play_pause = $("#play-pause");
        function pause(){
            $play_pause.attr("class","fas fa-play-circle fa-2x");
            video.pause();
        }
        function play(){
            if (!drawing){  // Dont allow play when drawing
                $play_pause.attr("class","fas fa-pause-circle fa-2x");
                video.play();
            }
        }
        $play_pause.click(function(){
            if (video.paused && !drawing){
                $play_pause.attr("class","fas fa-pause-circle fa-2x");
                video.play();
            }
            else{
                $play_pause.attr("class","fas fa-play-circle fa-2x");
                video.pause();
            }
        });
        return {
            pause: pause,
            play: play
        }
    })();

    /***
     * Audio controller module, controls game audio
     * @type {{stop, playSound}}
     */
    const audioController = (function(){
        const audio_correct = document.getElementById("audio-correct");
        const audio_incorrect = document.getElementById("audio-incorrect");
        let muted = false;
        function playSound(s){
            if(s==="correct"){
                if(!muted){audio_correct.play();}
                progressController.add_correct();
            }
            else if(s==="incorrect"){
                if(!muted){audio_incorrect.play();}
                progressController.add_incorrect();
            }
        }

        function toggleSound(element){
            console.log("joloo");
            muted = !muted;
            if(muted){element.setAttribute("class", "fas fa-volume-mute");}
            else{element.setAttribute("class", "fas fa-volume-up");}
        }

        return {
            playSound: playSound,
            toggleSound: toggleSound
        }

    })();

    volume_button.addEventListener("click", function(){audioController.toggleSound(volume_button)});

    /***
     * Progress controller module, controls the progress of the task.
     * @type {{removeLife}}
     */
    const progressController = (function(){
        // Create three lives
        let counter = 0;
        let container = document.getElementById("progress-container");

        function _game_ended(){
            statisticsController.fetch_and_set_stats();
            $question_root_container.hide();
            $("#scoreboard-container").hide();
            video.removeEventListener("loadeddata", video_ready);
            video.removeEventListener("ended", video_ended);
            videoController.pause();
            $video_labels_container.fadeOut("slow", function(){
                statisticsController.render();
            })
        }

        function add_correct(){
            counter += 1;
            let c = document.createElement("div");
            c.setAttribute("id", "correct");
            container.appendChild(c);
            if (counter===20){_game_ended();}
        }

        function add_incorrect(){
            counter += 1;
            let i = document.createElement("div");
            i.setAttribute("id","incorrect");
            container.appendChild(i);
            if (counter===20){_game_ended();}
        }

        return {
            add_correct: add_correct,
            add_incorrect: add_incorrect
        }

    })();

    /***
     * Game ended statistics controller, display statistics when game ends
     * @type {{fetch_and_set_stats, increment_score, render}}
     */

    const statisticsController = (function(){
        let $container = $("#game-ended-statistics");
        let placeholder = document.getElementById("placeholder");
        let score = 0;

        function render(){
            $container.slideDown();
        }

        function fetch_and_set_stats(){
            let d = JSON.stringify({
                "score": score
            });
            $.post("/api/score/", d).done(function(data){
                placeholder.innerHTML = "";  // Clear the placeholder
                if(!data.status){
                    let t0 = document.createElement("h1");
                    let t1 = document.createTextNode("You need to log in to get statistics");
                    t0.appendChild(t1);
                    placeholder.appendChild(t0);
                }
                else{
                    if(data.highscore) {
                        let t0 = document.createElement("h1");
                        let t1 = document.createTextNode("You made a new high score of: " + score);
                        t0.appendChild(t1);
                        placeholder.appendChild(t0);
                    }
                    let t2 = document.createElement("h1");
                    let t3 = document.createTextNode("You were better than "+data.compared_to_all+"% of the users");
                    t2.appendChild(t3);
                    placeholder.appendChild(t2);
                }
            })
        }
        function increment_score(){
            score++;
        }
        return {
            increment_score: increment_score,
            render: render,
            fetch_and_set_stats: fetch_and_set_stats
        }
    })();


    /*
    Canvas
     */
    const canvas = document.getElementById("canvas");
    const ctx = canvas.getContext("2d");
    canvas.width = 640;
    canvas.height = 360;
    let update_canvas_condition = true;
    function update_canvas(){
        if (update_canvas_condition){
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            requestAnimationFrame(update_canvas);
        }
    }


    let drawing = false;
    let draw_start_x = 0;
    let draw_start_y = 0;
    let draw_end_x = 0;
    let draw_end_y = 0;
    /**
     * mousedown event function, to start drawing bounding box on the canvas and pause the video
     * @param event
     */
    function draw_box_start(event){
        draw_start_x = event.offsetX;
        draw_start_y = event.offsetY;
        update_canvas_condition = false;
        drawing = true;
        videoController.pause();
        canvas.addEventListener('mousemove', draw_box_drawing);
    }
    canvas.addEventListener('mousedown',draw_box_start);

    /**
     * Draw the current preview of box, when moving mouse in canvas
     * @param event
     */
    function draw_box_drawing(event){
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        ctx.beginPath();
        ctx.lineWidth = "2";
        ctx.strokeStyle = "red";
        ctx.rect(draw_start_x, draw_start_y, event.offsetX-draw_start_x, event.offsetY-draw_start_y);
        ctx.stroke();
    }

    /**
     * mouseup event function, to end drawing bounding box and post the result to the server.
     * @param event
     */
    function draw_box_end(event){
        draw_end_x = event.offsetX;
        draw_end_y = event.offsetY;
        if(drawing) {
            post_box(Math.min(draw_start_x, draw_end_x), Math.min(draw_start_y, draw_end_y),
                Math.max(draw_start_x, draw_end_x), Math.max(draw_start_y, draw_end_y));
        }
        drawing = false;
        canvas.removeEventListener('mousemove', draw_box_drawing);
        videoController.play();
        update_canvas_condition = true;
        update_canvas();

    }
    canvas.addEventListener('mouseup', draw_box_end);

    /**
     * Attach escape listener to break drawing
     * @param event
     */
    window.onkeyup = function(event){
        if (event.key === "Escape"){
            drawing = false;
            draw_box_end(event);
        }
        else if (event.key === " "){
            if (video.paused) videoController.play();
            else videoController.pause();
        }
    };

    function post_box(min_x, min_y, max_x, max_y){
        let d = JSON.stringify({
            "task_id": segment_info.task_id,
            "time": video.currentTime + segment_info.startTime,
            "min_x": min_x,
            "min_y": min_y,
            "max_x": max_x,
            "max_y": max_y,
        });
        $.post("/api/task/", d).done(function(data){
            displayToast(data.message, 3000);
            audioController.playSound(data.sound);
            if(data.points === true){
                statisticsController.increment_score();
                draw_correct_and_incorrect(data.correct, data.incorrect);
                setTimeout(function(){new_task(video, question);},2000);
            }
            else{
                new_task(video, question);
            }
        });
    }

    /**
     * Draw cursors on the canvas.
     * @param green - list of green colored cursors
     * @param red - list of red colored cursors
     */
    function draw_correct_and_incorrect(green, red){
        update_canvas_condition = false;
        videoController.pause();
        for (let i = 0; i < green.length ; i++){
            ctx.beginPath();
            ctx.lineWidth = "1";
            ctx.strokeStyle = "green";
            ctx.rect(green[i].min_x, green[i].min_y, green[i].max_x-green[i].min_x, green[i].max_y-green[i].min_y);
            ctx.stroke();
        }
        for (let i = 0; i < red.length ; i++){
            ctx.beginPath();
            ctx.lineWidth = "1";
            ctx.strokeStyle = "red";
            ctx.rect(red[i].min_x, red[i].min_y, red[i].max_x-red[i].min_x, red[i].max_y-red[i].min_y);
            ctx.stroke();
        }
    }

    function set_question(container, question, img_hint){
        container.innerHTML = "";
        question_key = question;
        const description = document.createTextNode(question+"  ");
        container.appendChild(description);
        if (img_hint !== false){
            const i = document.createElement("img");
            i.src = img_hint;
            i.width = 45;
            i.height = 45;
            container.appendChild(i);
        }
        accept_clicks = true;
        $question_container.show();
    }

    function new_task(v, q){
        $question_container.hide();
        accept_clicks = false;
        update_canvas_condition = true;
        $.get("/api/task/").done(function(data){
            video.setAttribute("src", data.src);
            segment_info = {
                "task_id": data.task_id,
                "startTime": data.startTime,
                "endTime": data.endTime,
                "question": data.question,
                "img_hint": data.img_hint
            };
        }).done(function () {
            setTimeout(function(){set_question(q, segment_info.question, segment_info.img_hint)}, 1000);
        });
    }

    new_task(video, question);
    function video_ready(){
        video.muted = true;
        videoController.play();
        requestAnimationFrame(update_canvas);
    }
    video.addEventListener('loadeddata', video_ready);

    video.ontimeupdate = function(){
        let percentage = (video.currentTime / video.duration) * 100;
        $("#custom-seekbar span").css("width", percentage + "%");
    };
    function video_ended(){
        new_task(video, question);
        audioController.playSound("incorrect");
    }
    video.addEventListener("ended", video_ended);

    /**
     * Display toast message for duration of time
     * @param message message to be displayed
     * @param duration duration of the message in ms
     */
    function displayToast(message, duration){
        const d = document.getElementById("snackbar");
        d.innerHTML = message;
        if(accept_toast){
            accept_toast = false;
            d.className = "show";
            setTimeout(function(){
                d.className = d.className.replace("show","");
            }, duration);
            setTimeout(function(){ accept_toast = true}, 5000);  // Dont spam toast
        }
        else{

        }
    }
    /*
    Additional methods; cookies
     */
    // Set the crsf cookie
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
});
