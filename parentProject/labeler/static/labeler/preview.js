$(document).ready(function(){
    "use strict";
    const canvas = document.getElementById('myChart');
    const ctx = canvas.getContext('2d');
    let id = false;
    Chart.defaults.global.defaultFontColor='white';
    const _options = {
        legend: {
            labels: {
                fontSize: 52,
                fontColor: 'white'
            }
        },
        tooltips: {
            titleFontSize: 52,
            bodyFontSize: 52
        },
        scales: {
            xAxes: [{
                barThickness: 10
            }]
        }
    };

    let myChart=false;
    $(".list-group-item").click(function(){
        id = $(this).attr('id');
        $.get("/api/chartjs/answer/"+id+"/", function(data){
            $("#preview-video").show();
            const d = parse_data(data);
            myChart = create_chart(ctx, d, _options, myChart);
        });
    });

    $("#preview-video").click(function(){
        $.get("/api/get_annotations/"+id+"/", function(data){
            if(data.status){  // status: true, if all good
                myChart.destroy();
                $("#preview-video").hide();
                $("#preview-list").hide();
                $("#myChart").hide(function(){
                    $("#video-container").fadeIn(function(){
                        new PreviewVideo(640, 360, data.src, data.annotations);
                    });
                });
            }
        });
    });

    let mouseDown = false;  // Mouse is pressed inside canvas
    let xStart = 0;
    canvas.onmousedown = function(evt) {
       mouseDown = true;
       const activePoint = myChart.getElementAtEvent(evt)[0];
       console.log(activePoint);
    };

    canvas.onmouseup = function(evt) {
        if (mouseDown) {
            console.log("Dragged, started at: "+xStart);
        }
    };

    function array_from_range(start, end, hideHack=1){
        // hideHack is a hackaround for showing only part of the x-axis labels
        let r = [];
        for (let i=start; i <= end; i++){
            if (i % hideHack === 0) r.push(i.toString());
            else r.push("");
        }
        return r
    }

    function parse_data(data){
        let start = data["start"];
        let end = data["end"];
        delete data["start"];
        delete data["end"];
        let dynamicColors = function() {
           const r = Math.floor(Math.random() * 255);
           const g = Math.floor(Math.random() * 255);
           const b = Math.floor(Math.random() * 255);
           return "rgb(" + r + "," + g + "," + b + ")";
        };
        let ret = {
            datasets: []
        };
        for (let key in data) {
            const col = dynamicColors()
            const temp = {
                label: key,
                backgroundColor: dynamicColors(),
                data: data[key],
            };
            ret.datasets.push(temp);
        }
        ret["labels"] = array_from_range(start, end, 10);
        return ret;
    }

    function create_chart(context, data, options, prev_chart=false){
        if (prev_chart !== false) prev_chart.destroy();
        return new Chart(context, {
            type: "bar",
            data: data,
            options: options
        });
    }
});