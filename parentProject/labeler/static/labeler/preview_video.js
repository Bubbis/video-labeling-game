"use strict";
class PreviewVideo {
    constructor(width, height, src, annotations){
        this.canvas = document.getElementById("myVideo");
        this.canvas.width = width;
        this.canvas.height = height;
        this.annotations = annotations;
        this.src = src;
        this.ctx = this.canvas.getContext("2d");
        this.video = document.createElement("video");
        this.$play_pause = $("#play-pause");
        this.$custom_seekbar = $("#custom-seekbar");
        this.init_video();
        this.init_play_pause();
        this.init_seek_bar();
    }

    init_play_pause(){
        this.$play_pause.on("click", this.play_pause.bind(this));
    }

    init_seek_bar(){
        this.$custom_seekbar.on("click", this.set_video_time.bind(this));
    }

    set_video_time(e){
        let offset = $(e.currentTarget).offset();
        let left = (e.pageX - offset.left);
        let totalWidth = $("#custom-seekbar").width();
        let percentage = ( left / totalWidth );
        let vidTime = this.video.duration * percentage;
        this.video.currentTime = vidTime;
    }

    init_video(){
        this.video.setAttribute("src", this.src);
        this.video.addEventListener('loadeddata', this.video_ready.bind(this));
        this.video.addEventListener("ended", this.video_ended.bind(this));
        this.canvas.addEventListener("click", this.play_pause.bind(this));
    }

    play_pause(){
        if (this.video.paused){
            this.$play_pause.attr("class","fas fa-pause-circle fa-2x");
            this.video.play();
        }
        else{
            this.video.pause();
            this.$play_pause.attr("class","fas fa-play-circle fa-2x");
        }
    }

    video_ready(){
        this.video.muted = true;
        this.video.play();
        requestAnimationFrame(this.update_canvas.bind(this));
    }

    update_canvas(){
        this.ctx.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height);
        this.update_slider();
        const annotations = this.get_annotations(this.video.currentTime);
        for (let i in annotations) {
            this.draw_box(annotations[i]["point"]["min_x"], annotations[i]["point"]["min_y"],
                          annotations[i]["point"]["max_x"], annotations[i]["point"]["max_y"], annotations[i]["label"])
        }
        requestAnimationFrame(this.update_canvas.bind(this));
    }

    video_ended(){
        console.log("Video ended called");
    }

    /**
     * Get all all annotations for current time
     * @param time
     * @returns {Array}
     */
    get_annotations(time){
        let ret = [];
        for (let i in this.annotations){
            let first_t, second_t, first_p, second_p;
            const label = this.annotations[i]["label"];
            for (const [key, value] of Object.entries(this.annotations[i]["points"])){
                if (key < time){
                    first_t = key;
                    first_p = value;
                }
                if (key > time){
                    second_t = key;
                    second_p = value;
                    break;
                }
            }
            if(first_t !== undefined && second_t !== undefined){
                ret.push({"label": label,
                    "point": this.get_interpolated_coordinates(first_p, second_p,
                    second_t-first_t, time-first_t)});
            }
        }
        return ret;
    }

    /**
     * Interpolate using linear interpolation between p1 and p2: {
     *     "min_x": int,
     *     "min_y": int,
     *     "max_x": int,
     *     "max_y": int
     * }
     * @param p1 first points to interpolate
     * @param p2 second points to interpolate
     * @param time_distance total time between two points
     * @param time_delta time from first point to current time
     */
    get_interpolated_coordinates(p1, p2, time_distance, time_delta){
        return {
            "min_x": p1["min_x"] + (((p2["min_x"] - p1["min_x"]) / time_distance) * time_delta),
            "min_y": p1["min_y"] + (((p2["min_y"] - p1["min_y"]) / time_distance) * time_delta),
            "max_x": p1["max_x"] + (((p2["max_x"] - p1["max_x"]) / time_distance) * time_delta),
            "max_y": p1["max_y"] + (((p2["max_y"] - p1["max_y"]) / time_distance) * time_delta),
        }
    }

    draw_box(min_x, min_y, max_x, max_y, label){
        this.ctx.beginPath();
        this.ctx.lineWidth = "2";
        this.ctx.strokeStyle = "green";
        this.ctx.rect(min_x, min_y, max_x-min_x, max_y-min_y);
        this.ctx.font = "15px Arial";
        this.ctx.textAlign = "center";
        this.ctx.fillText(label, min_x + (max_x - min_x) / 2,min_y + (max_y - min_y) / 2);
        this.ctx.stroke();
    }

    update_slider(){
        let percentage = (this.video.currentTime / this.video.duration) * 100;
        $("#custom-seekbar span").css("width", percentage + "%");
    };
}