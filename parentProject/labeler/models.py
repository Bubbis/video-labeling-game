from django.db import models
from django.db.models.signals import pre_delete, post_save, post_delete, pre_save
from django.dispatch.dispatcher import receiver
from django.contrib.auth.models import User
from django.conf import settings  # Used to get the MEDIA_URL
from labeler.ffmpeg_functions import get_duration


# Extend the existing user model to have accuracy model
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    credits = models.IntegerField(default=0)
    experience = models.IntegerField(default=0)
    level = models.IntegerField(default=0)
    correct_suggestion = models.IntegerField(default=1)
    incorrect_suggestion = models.IntegerField(default=0)
    accuracy = models.FloatField(default=1)
    high_score = models.IntegerField(default=0)


# Handle user creation and edition
@receiver(post_save, sender=User)
def create_or_edit_profile(sender, instance, created, **kwargs):
    if created:
        prof = Profile.objects.create(user=instance)
        prof.save()
    else:  # Should be already created
        pass


class Video(models.Model):
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)  # Set to False when all labels have been set
    length = models.FloatField(blank=True, null=True)  # Length of the video in seconds
    videoFile = models.FileField(upload_to='')

    def __str__(self):
        return "Video: {} Id: {}".format(self.videoFile.name, self.id)


@receiver(post_save, sender=Video)
def init_label(created, instance, **kwargs):
    """
    When video is created, check the length and save to database
    TODO: Process the video in a way that every second has a keyframe.
    :param created: - If the file was just created
    :param instance: - Instance of the created model
    :param kwargs: - Additional parameters
    :return: - No return value
    """
    if created:
        duration = get_duration("{}{}".format(settings.MEDIA_ROOT, instance.videoFile.name))
        instance.length = duration
        instance.save()
    else:  # Label should be already made and length should not change
        pass


# Delete from filesystem when deleting from database
@receiver(pre_delete, sender=Video)
def delete_video_file(sender, instance, **kwargs):
    instance.videoFile.delete()


class Question(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    image_hint = models.ImageField(upload_to='', null=True, blank=True)


# Delete the image from filesystem
@receiver(pre_delete, sender=Question)
def delete_image_hint(sender, instance, **kwargs):
    instance.image_hint.delete()


class Task(models.Model):
    question = models.ForeignKey(Question, null=False, blank=False, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, null=False, blank=False, on_delete=models.CASCADE)
    start = models.IntegerField(null=False, blank=False)
    end = models.IntegerField(null=False, blank=False)


class Answer(models.Model):
    task = models.ForeignKey(Task, null=False, blank=False, on_delete=models.CASCADE)
    time = models.FloatField()
    min_x = models.IntegerField()
    min_y = models.IntegerField()
    max_x = models.IntegerField()
    max_y = models.IntegerField()