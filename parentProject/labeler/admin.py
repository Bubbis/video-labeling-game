from django.contrib import admin
from .models import Video, Question, Answer, Task


# Register your models here.
admin.site.register(Video)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Task)