# Helper functions for creating the annotation at api point
import math


def merge_bounding_box(box, new_box, total_merges):
    """
    Merge two bounding boxes, using running average.
    Box should be in format(dict):
    box={
        "min_x": int,
        "min_y": int,
        "max_x": int,
        "max_y": int
    }
    :param box: As dictionary
    :param new_box: As dictionary
    :param total_merges: integer for the total number of merges applied,
                        e.g amount of merges in box + 1(new_box)
    :return: Merged box
    """
    min_x = int((box["min_x"] * (total_merges - 1) + new_box["min_x"]) / total_merges)
    min_y = int((box["min_y"] * (total_merges - 1) + new_box["min_y"]) / total_merges)
    max_x = int((box["max_x"] * (total_merges - 1) + new_box["max_x"]) / total_merges)
    max_y = int((box["max_y"] * (total_merges - 1) + new_box["max_y"]) / total_merges)
    return {
        "min_x": min_x,
        "min_y": min_y,
        "max_x": max_x,
        "max_y": max_y
    }


def bounding_box_distance(box1, box2):
    """
    Calculate the distance between the center of two bounding boxes
    Box should be in format {
            "min_x": answer.min_x,
            "min_y": answer.min_y,
            "max_x": answer.max_x,
            "max_y": answer.max_y
        }
    :param box1:
    :param box2:
    :return: float as the distance between the centers
    """
    x1_mid = (box1["min_x"] + box1["max_x"]) / 2
    x2_mid = (box2["min_x"] + box2["max_x"]) / 2
    y1_mid = (box1["min_y"] + box1["max_y"]) / 2
    y2_mid = (box2["min_y"] + box2["max_y"]) / 2
    return math.hypot(x1_mid - x2_mid, y1_mid - y2_mid)


def find_parent_index_from_list(annotations, answer, max_second_distance=4, max_px_distance=100):
    """
    Find the index of item in the annotations list, tha that the item belongs to.
    Returns false if no parent is found, then we can create new annotation.
    Python 3.6+ dicts are insertion ordered. We insert items in frame order, so we treat them as sorted.(*)

    The algorithm works as follows:
        1. First we check that the annotation has the same label as the answer question
        2. Then we find the last key of the points dictionary. This dict should be in order(*).
            This key indicates the largest frame where the item has been currently seen
        3. Using this key we find the bounding box for the last frame this current item has been seen
        4. We calculate the pixel and frame distance to the answer we are trying to find parent to
        5. Then we check if the frame_distance or pixel_distance is larger than we should accept
            Note: just in case we also compare that the frame_distance is not smaller than 0
            Because we are inserting items in to the dictionary time orderly. It is a error check because,
            if this would happen, we would insert the item in wrong order(*).
    :param annotations:  List of the current annotations
    :param answer: Answer model from labeler.models to be found on annotations
    :return: -1 if False or int >= 0 stating the index
    """
    if len(annotations) == 0:
        return -1
    else:
        points_dict = {
            "min_x": answer.min_x,
            "min_y": answer.min_y,
            "max_x": answer.max_x,
            "max_y": answer.max_y
        }
        return_index = 0
        smallest_px_frame_sum = 0
        found_some_parent = False
        for index, annotation in enumerate(annotations):
            if answer.question.name != annotation["label"]:
                continue
            else:  # The annotation should be the same label
                s = list(annotation["points"].keys())[-1]
                p = annotation["points"][s]
                px_distance = bounding_box_distance(points_dict, p)
                second_distance = answer.time - s
                if second_distance < 0 or second_distance > max_second_distance or px_distance > max_px_distance:
                    continue
                else:
                    # print("Heh")
                    px_frame_sum = px_distance + (second_distance*10)
                    if found_some_parent is False:
                        return_index = index
                        smallest_px_frame_sum = px_frame_sum
                        found_some_parent = True
                    elif smallest_px_frame_sum > px_frame_sum:
                        return_index = index
                        smallest_px_frame_sum = px_frame_sum
        if found_some_parent is True:
            return return_index
        else:
            return -1
