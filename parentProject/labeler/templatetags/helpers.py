from django import template
register = template.Library()


@register.filter(name='decimal_to_time')
def decimal_to_time(seconds):
    """
    This function takes seconds as float and returns:
        minute:seconds:secondDecimals
    :param seconds: Float representing seconds
    :return string: Returns MM.SS[.xxxxx] format
    """
    minutes = int(seconds / 60)
    return "{}.{}".format(minutes, seconds-minutes*60)


