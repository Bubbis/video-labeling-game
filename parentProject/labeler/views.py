from random import randint
from math import ceil
from collections import defaultdict, OrderedDict
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.http import HttpResponse,  JsonResponse
from labeler.annotation_creation_functions import merge_bounding_box
from labeler.game_logic_functions import reward_profile, get_correct_and_incorrect_answers
from labeler.ffmpeg_functions import create_segment_of_video
from .forms import RegistrationForm
from django.forms import ValidationError
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings  # Used to get the MEDIA_URL
from .models import Video, Question, Answer, Profile, Task
import json


# Home view
def index(request):
    return render(request, "index.html")


# Memory game view
def play(request):
    if request.method == "GET":
        return render(request, "game.html")
    if request.method == "POST":
        pass


def preview(request):
    return render(request, "preview.html", {"videos": Video.objects.all()})


# Form for registering as user
def registration(request):
    """
    Register the new user, log in and redirect to index page
    Naive redirect to registration page if the username is taken :o
    :param request:
    :return:
    """
    if request.method == "GET":
        form = RegistrationForm()
        return render(request, 'registration.html', {'context': form})

    elif request.method == "POST":
        form = RegistrationForm(request.POST)
        # check that form is valid
        if form.is_valid():
            # Clean and get the data
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            username = form.cleaned_data['username']
            if password1 and password1 != password2:
                raise ValidationError("Password empty or not a match")
            if User.objects.filter(username=username).exists():
                return redirect('/registration')
            User.objects.create_user(username=username, password=password1)
            user = authenticate(request, username=username, password=password1)
            login(request, user)
            return redirect('/index')
        else:
            return redirect('/registration')


def api_get_annotations(request, video_id):
    """
    Get annotations in JSON format for given video.
    Note: You should use your own implementation on the raw database.. (Filters, etc)
    :param request:
    :param video_id:
    :return: {video_framerate: framerate,
              annotations: [{label: human,
                             points: {0.001: {min_x: 15,
                                           min_y: 6,
                                           max_x: 20,
                                           max_y: 25},
                                      5.213123: {min_x: 10,
                                          min_y: 10,
                                          max_x: 10,
                                          max_y: 10}
                                      }}]
    """
    if request.method == "GET":
        try:
            v = Video.objects.get(id=video_id)
        except Video.DoesNotExist:
            return JsonResponse({"status": False,
                                 "error": "Could not find video"})
        all_tasks = Task.objects.filter(video=v).order_by("start")
        annotations = []
        for task in all_tasks:
            answers = Answer.objects.filter(task=task).order_by("time")
            temp = {
                "label": task.question.name,
                "points": OrderedDict()
            }
            merge_counter = defaultdict(int)
            for answer in answers:
                point_dict = {
                    "min_x": answer.min_x,
                    "min_y": answer.min_y,
                    "max_x": answer.max_x,
                    "max_y": answer.max_y
                }
                if answer.time in temp["points"]:
                    merge_counter[answer.time] += 1
                    merged_box = merge_bounding_box(temp["points"][answer.time], point_dict, merge_counter[answer.time])
                    temp["points"][answer.time] = merged_box
                else:
                    merge_counter[answer.time] += 1
                    temp["points"][answer.time] = point_dict
            annotations.append(temp)
        return JsonResponse({
            "status": True,
            "src": "{}{}".format(settings.MEDIA_URL, v.videoFile.name),
            "annotations": annotations
        })


def api_task(request):
    """
    Api endpoint for requesting a new task and posting the results
    :param request:
    :return:
    """
    if request.method == "GET":
        task = Task.objects.all().order_by('?').first()
        if task is None:
            return JsonResponse({"status": False, "error": "Could not find any task"})
        else:  # randint(0, int(video.length) - length)
            if task.end - task.start < 10:
                start = task.start
                end = task.end
            else:
                start = randint(task.start, task.end - 10)
                end = start + 10
            if settings.LOCALDEV:  # FFmpeg implementation (local dev)
                prefix = "temp_"
                create_segment_of_video(settings.MEDIA_ROOT,
                                        task.video.videoFile.name,
                                        start, end,
                                        prefix)
                video_name = prefix + task.video.videoFile.name
            else:  # Nginx implementation (production)
                video_name = task.video.videoFile.name + "?start={}&end={}".format(start, end)
            if task.question.image_hint.name == "":
                hint = False
            else:
                hint = "{}{}".format(settings.MEDIA_URL, task.question.image_hint.name)
            return JsonResponse({
                'status': True,
                "task_id": task.id,
                'startTime': start,
                'endTime': end,
                "src": "{}{}".format(settings.MEDIA_URL, video_name),
                "question": task.question.name,
                "img_hint": hint
            })
    elif request.method == "POST" and request.is_ajax():
        data = json.loads(request.body.decode('utf-8'))
        try:
            t = Task.objects.get(id=data["task_id"])
        except Task.DoesNotExist:
            return JsonResponse({"status": False, "error": "Could not find task"})
        a = Answer.objects.create(task=t, time=data["time"], min_x=data["min_x"],
                                  min_y=data["min_y"], max_x=data["max_x"], max_y=data["max_y"])
        a.save()
        correct, incorrect = get_correct_and_incorrect_answers(a)
        ratio = len(correct) / (len(correct) + len(incorrect)) if len(correct) + len(incorrect) > 0 else 1
        if ratio > 0.5:  # Over half is correct
            if request.user.is_authenticated:
                reward_profile(request.user.profile, 10)
            return JsonResponse({"message": "Good job!",
                                 "sound": "correct",
                                 "points": True,
                                 "correct": correct,
                                 "incorrect": incorrect})
        else:
            if request.user.is_authenticated:
                request.user.profile.incorrect_suggestion += 1
                acc = request.user.profile.correct_suggestion / (request.user.profile.correct_suggestion +
                                                                 request.user.profile.incorrect_suggestion)
                request.user.profile.accuracy = acc
                request.user.profile.save()
            return JsonResponse({"message": "Try to be more accurate!",
                                 "sound": "incorrect"})


@login_required()
def api_manage_task(request, key=None):
    if not request.user.is_superuser:
        return HttpResponse("You are not superuser")
    else:
        if request.method == "GET" and not request.is_ajax():  # User requests the view
            return render(request, 'manage_tasks.html', context={"videos": Video.objects.all()})
        elif request.method == "GET" and request.is_ajax() and key is not None:  # User clicked video from list
            try:
                video = Video.objects.get(id=key)
                questions = Question.objects.all()
                if questions is None:
                    return JsonResponse({
                        "video_id": key,
                        "length": video.length,
                        "src": "{}{}".format(settings.MEDIA_URL, video.videoFile.name),
                        "questions": False,
                        "tasks": False
                    })
                else:
                    questions_list = []
                    for question in questions:
                        questions_list.append(question.name)
                tasks = Task.objects.filter(video=video)
                if tasks is None:
                    return JsonResponse({
                        "video_id": key,
                        "length": video.length,
                        "src": "{}{}".format(settings.MEDIA_URL, video.videoFile.name),
                        "questions": questions_list,
                        "tasks": False
                    })
                else:
                    tasks_list = []
                    for task in tasks:
                        tasks_list.append({
                            "question": task.question.name,
                            "start": task.start,
                            "end": task.end
                        })
                    return JsonResponse({
                        "video_id": key,
                        "length": video.length,
                        "src": "{}{}".format(settings.MEDIA_URL, video.videoFile.name),
                        "questions": questions_list,
                        "tasks": tasks_list
                    })
            except Video.DoesNotExist:
                return JsonResponse({"status": False, "error": "Could not find video"})
        elif request.method == "POST":  # User updates the tasks list
            # TODO: Fix that the new tasks are modified and not completely overwritten by the user..
            # Note: If the existing task start time is changed, the timestamps will go incorrectly from the answers.
            try:
                video = Video.objects.get(id=key)
                data = json.loads(request.body.decode('utf-8'))
                # We just remove all previous tasks and create new ones ...
                Task.objects.filter(video=video).delete()
                for new_task in data.values():
                    try:
                        q = Question.objects.get(name=new_task["question"])
                        Task.objects.create(video=video, question=q, start=int(float(new_task["start"])),
                                            end=int(float(new_task["end"])))
                    except Question.DoesNotExist:
                        return JsonResponse({"status": False, "error": "Corrupted question name"})
            except Video.DoesNotExist:
                return JsonResponse({"status": False, "error": "Could not video"})
            return JsonResponse({"status": True})


def api_chartjs_answer(request, video, start=0, end=None):
    """
    Provide the labels for given video from start to end, in a format for chartjs.
    Labels are grouped by a second, and rounded to the lowest second. Eg, 1.9second goes to 1 second mark.
    The answers are provided as a list where every index corresponds to a second.
    So, the array is filled with 0 if there is no action in the current second.
    :param request:
    :param video:
    :param start:
    :param end:
    :return:
    """
    if request.method == "GET":
        res = defaultdict(lambda: defaultdict(int))
        if end is None:
            answers = Answer.objects.filter(task__video=video, time__gte=start).order_by('time')
        else:
            answers = Answer.objects.filter(video__video=video, time__gte=start, time__lte=end).order_by('time')
        for answer in answers:
            res[answer.task.question.name][int(answer.time)] += 1
        # Flatten the dictionary, fill it with zeroes when no answers on specific time
        if end is None:
            end = 0
            for key, value in res.items():
                for time in value:
                    if time > end:
                        end = time
        final = {}
        for key in res:
            final[key] = []
            for i in range(start, end+1):
                if i in res[key]:
                    final[key].append(res[key][i])
                else:
                    final[key].append(0)
        final["start"] = start
        final["end"] = end
        return JsonResponse(final)
    else:
        pass


def api_score(request):
    if request.method == "POST" and request.is_ajax() and request.user.is_authenticated:
        data = json.loads(request.body.decode('utf-8'))
        score = data.get("score", 1)
        hs = request.user.profile.high_score
        ret = dict()
        if hs < score:
            request.user.profile.high_score = score
            ret["highScore"] = True
        else:
            ret["highScore"] = False
        better = len(Profile.objects.filter(high_score__lte=score))
        all = len(Profile.objects.all())
        ret["compared_to_all"] = ceil((better/all) * 100)
        ret["status"] = True
        request.user.profile.save()
        return JsonResponse(ret)
    else:
        return JsonResponse({"status": False,
                             "message": "You need to log in to see statistics"})


def api_high_scores(request, amount=10):
    if request.method == "GET" and request.is_ajax():
        profiles = Profile.objects.all().order_by("-high_score")[:amount]
        ret = {}
        c = 0
        for p in profiles:
            ret[c] = {
                "name": p.user.username,
                "high_score": p.high_score
            }
            c += 1
        return JsonResponse(ret)
    else:
        pass


def api_scoreboard(request, amount):
    """
    Retrun the top-amount players. Eg, amount=10, return Top10
    :param request:
    :param amount: Amount of the top players
    :return: JsonResponse of the player names, levels and experience
    """
    if request.method == "GET" and request.is_ajax():
        profiles = Profile.objects.all().order_by("-level")[:amount]  # top amount eg. top 10
        ret = {}
        c = 0
        for p in profiles:
            ret[c] = {"name": p.user.username,
                      "level": p.level}
            c += 1
        return JsonResponse(ret)
    else:
        pass


def api_player(request, username):
    if request.method == "GET" and request.is_ajax():
        try:
            profile = Profile.objects.get(user__username=username)
            return JsonResponse({"found": True,
                                 "username": username,
                                 "level": profile.level,
                                 "accuracy": "%.2f" % profile.accuracy,
                                 "contributions": profile.incorrect_suggestion+profile.correct_suggestion,
                                 "credits": profile.credits})
        except Profile.DoesNotExist:
            return JsonResponse({"found": False})
    else:
        pass
