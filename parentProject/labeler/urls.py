from django.urls import path, re_path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    re_path(r'^login/$', auth_views.LoginView.as_view()),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(next_page=views.index)),
    path('index/', views.index, name='index'),
    path('', views.index, name="index"),
    path('play/', views.play, name="play"),
    path('registration/', views.registration, name='index'),
    path('preview/', views.preview, name="preview"),
    path('api/task/', views.api_task, name="api_new_task"),
    path('api/manage/task/', views.api_manage_task, name="api_manage_task"),
    path('api/manage/task/<int:key>/', views.api_manage_task, name="api_manage_task"),
    path('api/get_annotations/<int:video_id>/', views.api_get_annotations, name="api_get_annotations"),
    path('api/score/', views.api_score, name="api_score"),
    path('api/high_scores/<int:amount>/', views.api_high_scores, name="api_high_scores"),
    path('api/scoreboard/<int:amount>/', views.api_scoreboard, name="api_scoreboard"),
    path('api/player/<str:username>/', views.api_player, name="api_player_profile"),
    path('api/chartjs/answer/<int:video>/', views.api_chartjs_answer, name="api_chartjs_answer")
]
