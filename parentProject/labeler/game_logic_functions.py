from .models import Answer
import math
VOTE_REWARD_VALUE = 2
SUGGEST_REWARD_VALUE = 10


def get_intersection_area(b1, b2):
    """
    :param b1: box as answer object
    :param b2: box as answer object
    :return: area of intersection
    """
    dx = min(b1.max_x, b2.max_x) - max(b1.min_x, b2.min_x)
    dy = min(b1.max_y, b2.max_y) - max(b1.min_y, b2.min_y)
    if dx >= 0 and dy >= 0:
        return dx * dy
    else:
        return 0


def get_area(answer):
    """
    Get area of answer
    :param answer: Answer type
    :return: float of area
    """
    return (answer.max_x - answer.min_x) * (answer.max_y - answer.min_y)


def get_correct_and_incorrect_answers(answer):
    """
    Check are colliding boxes, and return correct answers as IoU > 0.65.
    Else append to incorrect answer.
    :param answer:
    :return:
    """
    all_answers = Answer.objects.filter(task=answer.task, time__gte=answer.time-0.5, time__lte=answer.time+0.5)
    correct = []
    incorrect = []
    for a in all_answers:
        intersection = get_intersection_area(answer, a)
        if intersection == 0:
            incorrect.append({"min_x": a.min_x, "min_y": a.min_y,
                              "max_x": a.max_x, "max_y": a.max_y})
            continue
        iou = intersection / (get_area(answer) + get_area(a) - intersection)
        if iou > 0.65:
            correct.append({"min_x": a.min_x, "min_y": a.min_y,
                            "max_x": a.max_x, "max_y": a.max_y})
        else:
            incorrect.append({"min_x": a.min_x, "min_y": a.min_y,
                              "max_x": a.max_x, "max_y": a.max_y})
    return correct, incorrect


def reward_profile(profile, points):
    """
    Reward profile with points. Points are calculated to experience and credits using accuracy.
    :param profile: Profile to be rewarded
    :param points: Points to use as the base
    :return:
    """
    adjusted_experience = points * max(profile.accuracy, 0.5)
    adjusted_credits = points * max(profile.accuracy, 0.5)
    profile.credits = profile.credits + adjusted_credits
    level_up = int((profile.experience + adjusted_experience) / 100)  # Goes to 0 if under 100
    profile.level = profile.level + level_up  # Allows multiple levels on one round
    profile.experience = profile.experience + adjusted_experience - (level_up * 100)
    profile.correct_suggestion = profile.correct_suggestion + 1
    profile.accuracy = profile.correct_suggestion / (profile.correct_suggestion + profile.incorrect_suggestion)
    profile.save()


def punish_profile(profile, points):
    """
    Take away profiles credits. The more accurate the profile is the less credits it loses. Credits cant go below 0
    :param profile: Profile to be punished
    :param points: Points that are used to calculate the credits
    :return:
    """
    adjuster = 1 - profile.accuracy
    profile.credits = max(0, profile.credits - (adjuster * points))  # If you are accurate you lose less credits
    profile.incorrect_suggestion = profile.incorrect_suggestion + 1
    profile.accuracy = profile.correct_suggestion / (profile.correct_suggestion + profile.incorrect_suggestion)
    profile.save()


