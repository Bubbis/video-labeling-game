from django import forms
from labeler.models import Video


# Form for creating user
class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=149)
    password1 = forms.CharField(widget=forms.PasswordInput, label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Repeat your password")


# Form for uploading Video
class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ["name", "videoFile"]
