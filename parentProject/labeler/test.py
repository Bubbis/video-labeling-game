from django.test import TestCase
from django.contrib.auth.models import User
from labeler.annotation_creation_functions import merge_bounding_box
from labeler.game_logic_functions import *
from unittest.mock import MagicMock
from django.core.files import File
from django.conf import settings
import tempfile
import os
import shutil


class TestRewardSystem(TestCase):
    def setUp(self):
        self.test1 = User.objects.create_user("test1", "test1")
        self.test1.save()
        self.test2 = User.objects.create_user("test2", "test2")
        self.test2.save()

    def test_initialValues(self):
        self.assertIs(self.test1.profile.accuracy, 1)
        pass


class TestBoundingBoxMerge(TestCase):
    def setUp(self):
        self.dict1 = {
            "min_x": 5,
            "min_y": 5,
            "max_x": 10,
            "max_y": 10
        }
        self.dict2 = {
            "min_x": 10,
            "min_y": 10,
            "max_x": 20,
            "max_y": 20
        }
        self.dict12 = {  # Dict 1,2 merge with n=2. Returns (int) so rounded to lower
            "min_x": 7,
            "min_y": 7,
            "max_x": 15,
            "max_y": 15
        }

    def test_merge(self):
        self.assertEquals(merge_bounding_box(self.dict1, self.dict1, 2), self.dict1, "Merge with itself error")
        self.assertEquals(merge_bounding_box(self.dict1, self.dict1, 3), self.dict1, "Merge with itself error")
        self.assertEquals(merge_bounding_box(self.dict1, self.dict2, 2), self.dict12, "Merge with itself error")
        pass


"""
class BasicSetUpAndTest(TestCase):
    def setUp(self):
        settings.MEDIA_ROOT = tempfile.mkdtemp(dir=os.path.join(settings.BASE_DIR, 'media/'))  # Create tempdir for test
        fake_video = MagicMock(spec=File, name='FileMock')  # Fake video file
        fake_video.name = 'mock_video.mp4'
        video = Video(name="MockFile", length=200, videoFile=fake_video)
        video.save()

    def test_that_not_empty(self):
        self.assertIsNot(len(Video.objects.all()), 0)

    def tearDown(self):
        shutil.rmtree(settings.MEDIA_ROOT)  # Remove tempdir and content used for testing
"""
