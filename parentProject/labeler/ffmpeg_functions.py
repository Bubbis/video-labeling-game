import subprocess
import shlex
import os
import sys
import re
from django.conf import settings


def create_segment_of_video(path, video_file, start_time, end_time, prefix="temp_"):
    """
    This function splits the output_video to segment and
    overwrites existing temp_ segment.
    :param path: Used to describe the path to the file
    :param video_file: Used to describe the video filename
    :param start_time: Start time of the segment
    :param end_time: End time of the segment
    :param prefix: Optional: change the prefix of the filename
    :return return_value: Returns the ffmpeg value, should be 0 on success
    """
    length = end_time - start_time
    ffmpeg = subprocess.call([
        'ffmpeg',
        '-ss', str(start_time),  # Start time of segment
        '-i', path+video_file,  # Video file to segment
        '-t', str(length),  # Length of the segment
        '-y',  # Overwrite output files without asking.
        '-c', 'copy',  # Copy codes
        '{}{}{}'.format(path, prefix, video_file),  # Name of the output
    ])
    return ffmpeg  # Should return 0 on success


def get_duration(file):
    """
    Get the duration of a video using ffprobe.
    https://stackoverflow.com/questions/31024968/using-ffmpeg-to-obtain-video-durations-in-python
    """
    if settings.TESTING:
        return 100
    cmd = 'ffprobe -i {} -show_entries format=duration -v quiet -of csv="p=0"'.format(file)
    args = shlex.split(cmd)
    output = subprocess.check_output(
        args,
        shell=False,  # shell=True is security hazard
        stderr=subprocess.STDOUT
    )
    return float(output)


def get_framerate(filename):
    """
    Determine the framerate of file
    https://askubuntu.com/questions/110264/how-to-find-frames-per-second-of-any-video-file
    :param filename: filepath
    :return: Framerate or false otherwise
    """
    if not os.path.exists(filename):
        sys.stderr.write("ERROR: filename %r was not found!" % (filename,))
        return False
    out = subprocess.check_output(["ffprobe", filename, "-v", "0", "-select_streams",
                                   "v", "-print_format", "flat", "-show_entries",
                                   "stream=r_frame_rate"])
    rate = str(out).split('=')[1].strip()[1:-1].split('/')  #Some string manipulation
    rate[1] = re.sub("\D", "", rate[1])  # Remove cluttering characters
    if len(rate) == 1:
        return float(rate[0])
    if len(rate) == 2:
        return float(rate[0])/float(rate[1])
    return False
