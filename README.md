# Labeler Game
Small game for building bounding box annotations to videos. 
Implemented using Python 3.6, Django, NGINX and FFmpeg.

- [Requirements](#requirements) - Requirements for the game
- [Running the website](#running-the-website) - Launching the game in local development or deploying to production.
- [Task management](#task-management) – How to create and edit tasks, questions, and videos
- [Gameplay](#gameplay) – Playing the game
- [Labels](#labels) - How to fetch and create models from user answers
- [Video examples](#video-examples) - Video captures displaying a game flow and task management flow
- [Known problems](#known-problems) - Bugs and problems found out, yet to be fixed.

# Requirements:
- Tested with python 3.6+
- FFmpeg for determining video lengths and segmentation in local dev.
- NGINX with http4-mp4-module (NGINX-extras) for pseudo-streaming in production
- Additional python modules from requirements.txt

# Running the website

## Local development:
- activate virtualenv and install requirements: pip install -r requirements.txt
- ./parentProject/$ python manage.py runserver
- Go to http://localhost:8000/index in the web browser

## Deploying to production:
- Install NGINX with http4-mp4-module (NGINX-extras)
- Set Debug=False and read secret-key from a file ( Not included in git )
- run uwsgi with: ./parentProject/$ uwsgi --ini uwsgi.ini
- run NGINX
    1. Connect to the uwsgi socket
    2. Serve static files and media files from the hard drive
    3. Pass other requests to the uwsgi socket. Example conf can be found at ./parentProject/nginx.conf

# Task management
- Create a superuser account with ./parentProject/$ python manage.py createsuperuser
- Navigate to /admin/
    1. You can add and edit tasks, questions, profiles, and videos here
    2. See the database diagram for an overview of the database structure
- You don't have to specify the length of a video, FFmpeg is used to calculate the video length.
- You can edit the tasks also in /api/manage/task/ at the browser. EDITING TASKS FROM THIS PAGE REMOVES ALL ANSWERS
IN THE VIDEO
    1. You can use set time buttons to set start and end times for a task
    2. Create new tasks using Create button
    3. When you are happy with the tasks post the new tasks to the server with Update.
    4. You should see a snackbar saying "updated" if everything went OK.


<img src="https://i.imgur.com/hHNbKtz.png" width=330 height=270>

# Labels
- To preview answers and the labels in video registered users can go to /preview/ and click on a video.
    1. Then from the chart, you can see the density of answers. By clicking the preview button right side of the chart video player is opened with the annotations.
- You can request annotations in JSON format for specific video_id from the endpoint: /api/get_annotations/video_id/
    1. Zero area answers are removed and other answers are gathered(merged in by average if in the same frame) and returned 
- OR you can write a script to interact with the SQLite database. 

# Gameplay
- A random task is selected and a 10-second clip from the task is displayed to the user.
- The user draws a rectangle around the object pointed by the task
- User answer is compared against other answers near (0.5-seconds) the answer. Intersection over union (IoU) 
is used to determine if the user is correct. The user is correct if over half of the answers nearby have
intersection over union more than 60%
- After 20 questions the game ends. For every correct answer, the user gets one point.


# Video examples:
- Game flow video:
https://drive.google.com/file/d/1BgTszcYPkThYtQxOCLplZyOFXqgF7wLM/view?usp=sharing

- Add and manage task, videos, and questions:
https://drive.google.com/file/d/1-pyPmmLWYTuW_dOgeuh4vfo81O0d1Df7/view?usp=sharing

# Known problems
-When editing tasks in manage tasks page all answers for the video are deleted.
This is because when updating tasks all previous tasks are deleted and new ones are created.
Fixing this requires also changes in the answer structure, as timestamps are looked calculated using task.start + answer.time
(Changing the task start time would distortion the answer.time + task.start formula)

-In local development, you have to use firefox to seek videos. Chrome does not store the whole
video in the buffer and local development does not support byte-range requests.